//
//  PhotoRepositorySpec.swift
//  WerkinApp
//
//  Created by Γιώργος Καϊμακάς on 07/06/16.
//  Copyright © 2016 gkaimakas. All rights reserved.
//

import Quick
import Nimble
import Foundation
import ReactiveCocoa
import Result
@testable import WerkinAppModels

class PhotoRepositorySpec: QuickSpec {
	override func spec() {
		let stack = WerkinAppModelStack()
		let repo = PhotoRepository(stack: stack)
		
		describe("fetchCloudPhotos") {
			it("should fetch a response") {
				var response: ResponseList<PhotoDTO>? = nil
				
				repo
					.fetchCloudPhotos()
					.on(next: { result in
						response = result
					})
					.start()
				
				expect(response).toEventuallyNot(beNil(), timeout: 25)
				if let response = response {
					expect(response.statusCode).toEventually(equal(200))
					expect(response.result.count).toEventually(equal(5000))
				}
			}
		}
		
		describe("fetchPhotos") {
			it("should fetch photos from cache or the cloud") {
				var photos: [Photo] = []
				
				repo
					.fetchPhotos()
					.on(next: { result in
						photos = result
					})
					.start()
				
				expect(photos.count).toEventually(equal(20), timeout: 50)
			}
		}
	}
}

