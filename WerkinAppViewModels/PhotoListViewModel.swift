//
//  PhotoListViewModel.swift
//  WerkinApp
//
//  Created by Γιώργος Καϊμακάς on 08/06/16.
//  Copyright © 2016 gkaimakas. All rights reserved.
//

import Foundation
import ReactiveCocoa
import Result
import Rex
import WerkinAppModels

public class PhotoListViewModel {
	private let repository: PhotoRepository
	
	public var currentPage: MutableProperty<Int>
	public var hasMoreEntries: MutableProperty<Bool>
	public var didRefreshEntries: MutableProperty<()>
	public var limit: ConstantProperty<Int>
	public var isLoadingData: MutableProperty<Bool>
	
	public var entries: [PhotoViewModel]
	
	public init(repository: PhotoRepository) {
		self.repository = repository
		
		currentPage = MutableProperty(0)
		limit = ConstantProperty(50)
		hasMoreEntries = MutableProperty(true)
		isLoadingData = MutableProperty(false)
		didRefreshEntries = MutableProperty()
		
		entries = []
	}
	
	public func fetchPhotos() -> SignalProducer<[PhotoViewModel], NSError> {
		return repository
			.fetchPhotos(page: currentPage.value, limit: limit.value)
			.observeOn(QueueScheduler.mainQueueScheduler)
			.on(started: {
				self.isLoadingData.value = true
			})
			.on(next: { result in
				self.currentPage.value = self.currentPage.value + 1
				self.hasMoreEntries.value = (result.count > 0)
			})
			.on(completed: {
				self.isLoadingData.value = false
			})
			.on(failed: { _ in
				self.isLoadingData.value = false
			})
			.map() { list in
				return list.map() { PhotoViewModel(repository:self.repository, photo: $0) }
			}
	}
	
	public func refresh() -> SignalProducer<(), NSError> {
		return repository
			.clearCache()
			.observeOn(QueueScheduler.mainQueueScheduler)
			.on(started: { self.isLoadingData.value = true })
			.on(next: { _ in
				self.entries.removeAll()
				self.didRefreshEntries.value = ()
				self.currentPage.value = 0
				self.hasMoreEntries.value = true
			})
			.flatMap(.Latest) { _ in
				return self
					.repository
					.fetchCloudPhotos()
			}
			.map() { _ in () }
	}
}
