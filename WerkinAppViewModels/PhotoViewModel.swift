//
//  PhotoViewModel.swift
//  WerkinApp
//
//  Created by Γιώργος Καϊμακάς on 08/06/16.
//  Copyright © 2016 gkaimakas. All rights reserved.
//

import Foundation
import ReactiveCocoa
import Result
import Rex
import WerkinAppModels

public class PhotoViewModel {
	private let repository: PhotoRepository
	private let photo: Photo
	
	public let id: ConstantProperty<Int16>
	public let albumId: ConstantProperty<Int16>
	public let title: ConstantProperty<String>
	public let url: ConstantProperty<NSURL?>
	public let thumbnailURL: ConstantProperty<NSURL?>
	
	public init(repository: PhotoRepository, photo: Photo) {
		self.repository = repository
		self.photo = photo
		
		id = ConstantProperty(photo.id)
		albumId = ConstantProperty(photo.albumId)
		title = ConstantProperty(photo.title)
		url = ConstantProperty(photo.url)
		thumbnailURL = ConstantProperty(photo.thumbnailURL)
	}
}
