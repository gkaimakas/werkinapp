//
// Created by Γιώργος Καϊμακάς on 08/06/16.
// Copyright (c) 2016 gkaimakas. All rights reserved.
//

import Alamofire
import AlamofireImage
import Foundation
import ReactiveCocoa
import Result
import UIKit
import WerkinAppViewModels

public class PhotoEntryCollectionViewCell: UICollectionViewCell {
    public static let Identifier = String(PhotoEntryCollectionViewCell)
    public static let Nib = UINib(nibName: String(PhotoEntryCollectionViewCell), bundle: NSBundle(forClass: PhotoEntryCollectionViewCell.self))
	public static let PlaceholderImage =
		UIImage(named: "ic_photo_36pt", inBundle: NSBundle(forClass: PhotoEntryCollectionViewCell.self), compatibleWithTraitCollection: nil)
	
	@IBOutlet weak var placeholderImageView: UIImageView!
	@IBOutlet weak var idLabel: UILabel!
	@IBOutlet weak var photoImageView: UIImageView!
	@IBOutlet weak var activityIndicator: UIActivityIndicatorView!
	@IBOutlet weak var titleLabel: UILabel!
	
	private var idDisposable: Disposable? = nil
	private var titleDisposable: Disposable? = nil
	private var urlDisposable: Disposable? = nil
	
	public var viewModel: PhotoViewModel? = nil {
		didSet {
			if let viewModel = viewModel {
				
				idDisposable = viewModel
					.id
					.producer
					.on(next: { result in
						self.idLabel.text = String(result)
					})
					.start()
				
				titleDisposable = viewModel
					.title
					.producer
					.on(next: {result in
						self.titleLabel.text = result
					})
					.start()
				
				urlDisposable = viewModel.url
					.producer
					.ignoreNil()
					.on(next: { result in
						self.activityIndicator.startAnimating()
						self.photoImageView.af_cancelImageRequest()
						self.placeholderImageView.hidden = false
						self.photoImageView
							.af_setImageWithURL(result,
								placeholderImage: nil,
								filter: nil,
								progress: nil,
								imageTransition: UIImageView.ImageTransition.CrossDissolve(0.5),
								runImageTransitionIfCached: false) { response in
									self.activityIndicator.stopAnimating()
									self.placeholderImageView.hidden = true
							}
						
					})
					.start()
				
			}
		}
	}
	
	override public func awakeFromNib() {
		super.awakeFromNib()
		
		backgroundColor = UIColor.clearColor()
		contentView.backgroundColor = UIColor.clearColor()
		placeholderImageView.image = PhotoEntryCollectionViewCell.PlaceholderImage
	}
	
	override public func prepareForReuse() {
		super.prepareForReuse()
		
		idDisposable?.dispose()
		titleDisposable?.dispose()
		urlDisposable?.dispose()
		
		activityIndicator.stopAnimating()
		titleLabel.text = nil
		photoImageView.af_cancelImageRequest()
		photoImageView.image = nil
		photoImageView.contentMode = .Center
	}
	
}
