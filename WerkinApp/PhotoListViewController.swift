//
//  ViewController.swift
//  WerkinApp
//
//  Created by Γιώργος Καϊμακάς on 08/06/16.
//  Copyright © 2016 gkaimakas. All rights reserved.
//

import Device
import ReactiveCocoa
import Result
import Rex
import UIKit
import WerkinAppViewModels
import WerkinAppViews

class PhotoListViewController: UIViewController {
	@IBOutlet weak var collectionView: UICollectionView!
	@IBOutlet weak var refreshBarButtonItem: UIBarButtonItem!
	@IBOutlet weak var noEntriesImageView: UIImageView!
	
	private var viewModel: PhotoListViewModel? = nil

	override func viewDidLoad() {
		super.viewDidLoad()
		// Do any additional setup after loading the view, typically from a nib.
		
		if let appDelegate = UIApplication.sharedApplication().delegate as? AppDelegate {
			viewModel = appDelegate.container.resolve(PhotoListViewModel)
			
			viewModel?
				.didRefreshEntries
				.producer
				.on(next: { _ in
					self.collectionView.reloadData()
				})
				.start()
			
			viewModel?
				.isLoadingData
				.producer
				.on(next: { result in
					if result == true {
						self.refreshBarButtonItem.enabled = false
						self.noEntriesImageView.hidden = false
					}
					
					if result == false {
						self.refreshBarButtonItem.enabled = true
						self.noEntriesImageView.hidden = true
					}
				})
				.start()
		}
		
		collectionView.registerNib(PhotoEntryCollectionViewCell.Nib, forCellWithReuseIdentifier: PhotoEntryCollectionViewCell.Identifier)
		collectionView.registerClass(UICollectionViewCell.self, forCellWithReuseIdentifier: String(UICollectionViewCell))
		collectionView.dataSource = self
		collectionView.delegate = self
		
		// ------------------- //
		
		navigationItem.title = "Photos"
	}
	
	override func viewDidAppear(animated: Bool) {
		super.viewDidAppear(animated)
	}

	override func didReceiveMemoryWarning() {
		super.didReceiveMemoryWarning()
		// Dispose of any resources that can be recreated.
	}
	
	func fetchPhotos() {
		viewModel?
			.fetchPhotos()
			.on(next: { results in
				self.collectionView.performBatchUpdates({
					
					var indexPaths: [NSIndexPath] = []
					
					for result in results {
						self.viewModel!.entries.append(result)
						let indexPath = NSIndexPath(forItem: self.viewModel!.entries.count-1, inSection: 0)
						indexPaths.append(indexPath)
					}
					
					self.collectionView.insertItemsAtIndexPaths(indexPaths)
					}, completion: nil)
			})
			.start()
	}
	
	override func didRotateFromInterfaceOrientation(fromInterfaceOrientation: UIInterfaceOrientation) {
		collectionView.performBatchUpdates(nil, completion: nil)
	}
	
	@IBAction func handleRefresh(sender: AnyObject) {
		viewModel?.refresh()
			.start()
	}
}

extension PhotoListViewController: UICollectionViewDataSource {
	func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
		return 2
	}
	
	func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {

		if section == 1 {
			return 1
		}
		
		if let viewModel = viewModel {
			return viewModel.entries.count
		}
		
		return 0
	}
	
	func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
		
		
		if indexPath.section == 1 {
			fetchPhotos()
			return collectionView.dequeueReusableCellWithReuseIdentifier(String(UICollectionViewCell), forIndexPath: indexPath)
		}
		
		
		let cell = collectionView.dequeueReusableCellWithReuseIdentifier(PhotoEntryCollectionViewCell.Identifier, forIndexPath: indexPath) as! PhotoEntryCollectionViewCell
		
		guard let viewModel = viewModel else {
			return cell
		}
		
		let entryViewModel = viewModel.entries[indexPath.item]
		cell.viewModel = entryViewModel
		return cell
	}
}

extension PhotoListViewController: UICollectionViewDelegateFlowLayout {
	func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
		
		if indexPath.section == 1 {
			return CGSizeMake(collectionView.bounds.width, 10)
		}
		
		var gridItems = 2
		
		if Device.isLargerThanScreenSize(Size.Screen5_5Inch) {
			gridItems = 3
		}
		
		if Device.isLargerThanScreenSize(Size.Screen7_9Inch) {
			gridItems = 4
		}
		
		if Device.isLargerThanScreenSize(Size.Screen12_9Inch) {
			gridItems = 5
		}
		
		if UIDeviceOrientationIsLandscape(UIDevice.currentDevice().orientation) {
			gridItems = gridItems * 2
		}
		
		let width = collectionView.bounds.width / CGFloat(gridItems)
		let height = width
		
		return CGSizeMake(width, height)
	}
	
	func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAtIndex section: Int) -> CGFloat {
		return 0
	}
	
	func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAtIndex section: Int) -> UIEdgeInsets {
		return UIEdgeInsetsZero
	}
	
	func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAtIndex section: Int) -> CGFloat {
		return 0
	}
}
