//
//  PhotoRepository.swift
//  WerkinApp
//
//  Created by Γιώργος Καϊμακάς on 07/06/16.
//  Copyright © 2016 gkaimakas. All rights reserved.
//

import Alamofire
import Foundation
import ReactiveCocoa
import Result
import SwiftyJSON

public class PhotoRepository {
	private let stack: WerkinAppModelStack
	
	public init(stack: WerkinAppModelStack) {
		self.stack = stack
		
	}
	
	public func cachePhotos(photos: [PhotoDTO]) -> SignalProducer<[Photo], NSError> {
		return SignalProducer { (observer, disposable) in
			do {
				var photoMOs: [Photo] = []
				
				for photo in photos {
					let photoMO = try self.stack.fetchOrCreatePhotoWithId(Int16(photo.id))
					photoMO.inflateWithPhotoDTO(photo)
					photoMOs.append(photoMO)
				}
				
				try self.stack.managedObjectContext.save()
				observer.sendNext(photoMOs)
				observer.sendCompleted()
				
			} catch let error as NSError {
				observer.sendFailed(error)
			}
		}
	}
	
	public func clearCache() -> SignalProducer<(), NSError> {
		return SignalProducer{ (observer, disposable) in
			do {
				
				let cachedPhotos = try self.stack.fetchAllPhotos()
				
				for entry in cachedPhotos {
					self.stack.managedObjectContext.deleteObject(entry)
				}
				
				try self.stack.managedObjectContext.save()
				observer.sendNext()
				observer.sendCompleted()
				
			} catch let error as NSError {
				observer.sendFailed(error)
			}
		}
	}
	
	public func fetchCloudPhotos() -> SignalProducer<ResponseList<PhotoDTO>, NSError> {
		return SignalProducer { (observer, disposable) in
			Alamofire.request(PhotoRouter.Photos).responseJSON() { response in
				if let error = response.result.error {
					observer.sendFailed(error)
				}
				
				if response.result.isSuccess {
					if let responseList = ResponseList<PhotoDTO>(response: response) {
						observer.sendNext(responseList)
						observer.sendCompleted()
					} else {
						observer.sendFailed(Errors.ResponseSerializableError)
					}
				}
			}
		}
	}
	
	public func fetchLocalPhotos(page page: Int = 0, limit: Int = 20) -> SignalProducer<[Photo], NSError> {
		return SignalProducer{ (observer, disposable) in
			do {
				
				let cachedPhotos = try self.stack.fetchPhotos(page: page, limit: limit)
				
				observer.sendNext(cachedPhotos)
				observer.sendCompleted()
				
				
			} catch let error as NSError {
				observer.sendFailed(error)
			}
		}
	}
	
	public func fetchPhotos(page page: Int = 0, limit: Int = 20) -> SignalProducer<[Photo], NSError> {
		return fetchLocalPhotos(page: page, limit: limit)
			.flatMap(.Latest) { result -> SignalProducer<[Photo], NSError> in
				if result.count == 0 {
					return self.fetchCloudPhotos()
						.map() { $0.result }
						.flatMap(.Latest) { fetchedPhotos in
							return self.cachePhotos(fetchedPhotos)
						}
						.flatMap(.Latest) { _ in
							return self.fetchLocalPhotos(page: page, limit: limit)
						}
				}
				
				return SignalProducer(value: result)
			}
	}
}