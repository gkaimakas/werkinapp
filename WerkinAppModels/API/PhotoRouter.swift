//
//  PhotoRouter.swift
//  WerkinApp
//
//  Created by Γιώργος Καϊμακάς on 07/06/16.
//  Copyright © 2016 gkaimakas. All rights reserved.
//


import Alamofire
import Foundation

enum PhotoRouter: URLRequestConvertible {
	static let BaseURL = "http://jsonplaceholder.typicode.com"
	
	static let PathPhotos = "/photos"
	
	case Photos
	
	var requestConfiguration: (path: String, verb: Alamofire.Method, params: [String:AnyObject]?) {
		switch self {
		case .Photos:
			return (path: PhotoRouter.PathPhotos, verb: .GET, params: nil)
		}
	}
	
	var URLRequest: NSMutableURLRequest {
		let result = self.requestConfiguration
		
		let URL = NSURL(string: PhotoRouter.BaseURL)!
		let URLRequest = NSMutableURLRequest(URL: URL.URLByAppendingPathComponent(result.path))
		
		URLRequest.HTTPMethod = result.verb.rawValue
		
		let encoding = Alamofire.ParameterEncoding.JSON
		
		return encoding.encode(URLRequest, parameters: result.params).0
	}
}