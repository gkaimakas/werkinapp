//
//  PhotoDTO.swift
//  WerkinApp
//
//  Created by Γιώργος Καϊμακάς on 07/06/16.
//  Copyright © 2016 gkaimakas. All rights reserved.
//

import Foundation
import SwiftyJSON

public struct PhotoDTO: JSONSerializable {
	static let AlbumId = "albumId"
	static let Id = "id"
	static let Title = "title"
	static let URL = "url"
	static let ThumbnailURL = "thumbnailUrl"
	
	public let albumId: Int
	public let id: Int
	public let title: String
	public let url: NSURL
	public let thumbnailURL: NSURL
	
	public init (id: Int, albumId: Int, title: String, url: NSURL, thumbnailURL: NSURL) {
		self.id = id
		self.albumId = albumId
		self.title = title
		self.url = url
		self.thumbnailURL = thumbnailURL
	}
	
	public init?(json: JSON) {
		guard let albumId = json[PhotoDTO.AlbumId].int,
			let id = json[PhotoDTO.Id].int,
			let title = json[PhotoDTO.Title].string,
			let urlString = json[PhotoDTO.URL].string,
			let thumbnailURLString = json[PhotoDTO.ThumbnailURL].string,
			let url = NSURL(string: urlString),
			let thumbnailURL = NSURL(string: thumbnailURLString) else {
			return nil
		}
		
		self.init(id: id, albumId: albumId, title: title, url: url, thumbnailURL: thumbnailURL)
	}
}