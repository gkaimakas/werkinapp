//
//  JSONSerializable.swift
//  WerkinApp
//
//  Created by Γιώργος Καϊμακάς on 07/06/16.
//  Copyright © 2016 gkaimakas. All rights reserved.
//

import Foundation
import SwiftyJSON

public protocol JSONSerializable {
	init?(json: JSON)
}
