//
//  Response.swift
//  WerkinApp
//
//  Created by Γιώργος Καϊμακάς on 07/06/16.
//  Copyright © 2016 gkaimakas. All rights reserved.
//

import Alamofire
import Foundation
import SwiftyJSON

public struct ResponseList<T: JSONSerializable>: ResponseSerializable{
	public let statusCode: Int
	public let result: [T]
	
	public init(statusCode: Int, result: [T]) {
		self.statusCode = statusCode
		self.result = result
	}
	
	public init?(response: Response<AnyObject, NSError>) {
		guard let code = response.response?.statusCode else {
			return nil
		}
		
		guard let value = response.result.value,
			let jsonArray = JSON(value).array else {
			return nil
		}
		
		var entries: [T] = []
		
		for json in jsonArray {
			if let entry = T(json: json) {
				entries.append(entry)
			}
		}
		
		self.init(statusCode: code, result: entries)
	}
	
}
