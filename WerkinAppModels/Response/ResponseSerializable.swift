//
//  ResponseSerializable.swift
//  WerkinApp
//
//  Created by Γιώργος Καϊμακάς on 07/06/16.
//  Copyright © 2016 gkaimakas. All rights reserved.
//

import Alamofire
import Foundation

public protocol ResponseSerializable {
	init? (response: Response<AnyObject, NSError>)
}