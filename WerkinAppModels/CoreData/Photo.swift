//
//  Photo.swift
//  WerkinApp
//
//  Created by Γιώργος Καϊμακάς on 08/06/16.
//  Copyright © 2016 gkaimakas. All rights reserved.
//

import Foundation
import CoreData

public class Photo: NSManagedObject {
	@NSManaged public var id: Int16
	@NSManaged public var albumId: Int16
	@NSManaged public var title: String
	@NSManaged public var urlString: String
	@NSManaged public var thumbnailURLString: String
	
	public var thumbnailURL: NSURL? {
		get {
			return NSURL(string: thumbnailURLString)
		}
		
		set {
			if let string = newValue?.absoluteString {
				thumbnailURLString = string
			}
		}
	}
	
	public var url: NSURL? {
		get {
			return NSURL(string: urlString)
		}
		
		set {
			if let string = newValue?.absoluteString {
				urlString = string
			}
		}
	}
	
	public func inflateWithPhotoDTO(photoDTO: PhotoDTO) {
		id = Int16(photoDTO.id)
		albumId = Int16(photoDTO.albumId)
		title = photoDTO.title
		thumbnailURL = photoDTO.thumbnailURL
		url = photoDTO.url
	}

}
