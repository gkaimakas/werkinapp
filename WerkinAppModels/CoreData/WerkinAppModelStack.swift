//
//  WerkinAppModelStack.swift
//  WerkinApp
//
//  Created by Γιώργος Καϊμακάς on 08/06/16.
//  Copyright © 2016 gkaimakas. All rights reserved.
//

import CoreData
import Foundation

public class WerkinAppModelStack: NSObject {
	
	public var managedObjectContext: NSManagedObjectContext
	public var coordinator: NSPersistentStoreCoordinator
	
	public override init() {
		guard let modelURL = NSBundle(forClass: self.dynamicType).URLForResource("WerkinAppModel", withExtension: "momd") else {
			fatalError("Error loading model from bundle")
		}
		
		guard let managedObjectModel = NSManagedObjectModel(contentsOfURL: modelURL) else {
			fatalError("Error initializing managedObjectModel from: \(modelURL)")
		}
		
		managedObjectContext = NSManagedObjectContext(concurrencyType: .MainQueueConcurrencyType)
		
		coordinator = NSPersistentStoreCoordinator(managedObjectModel: managedObjectModel)
		
		let urls = NSFileManager.defaultManager().URLsForDirectory(.DocumentDirectory, inDomains: .UserDomainMask)
		let applicationDocumentsDirectory = urls[urls.count-1]
		
		let url = applicationDocumentsDirectory.URLByAppendingPathComponent("WerkinAppModel.sqlite")
		
		let failureReason = "There was an error creating or loading the application's saved data."
		do {
			try coordinator.addPersistentStoreWithType(NSSQLiteStoreType, configuration: nil, URL: url, options: nil)
		} catch {
			// Report any error we got.
			var dict = [String: AnyObject]()
			dict[NSLocalizedDescriptionKey] = "Failed to initialize the application's saved data"
			dict[NSLocalizedFailureReasonErrorKey] = failureReason
			
			dict[NSUnderlyingErrorKey] = error as NSError
			let wrappedError = NSError(domain: "YOUR_ERROR_DOMAIN", code: 9999, userInfo: dict)
			// Replace this with code to handle the error appropriately.
			// abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
			NSLog("Unresolved error \(wrappedError), \(wrappedError.userInfo)")
			abort()
		}
		managedObjectContext.persistentStoreCoordinator = coordinator
		super.init()
	}
	
	//MARK: - Convenience functions for FetchRequests
	
	
	public func fetchAllPhotos() throws -> [Photo] {
		let request = NSFetchRequest(entityName: String(Photo))
		request.sortDescriptors = [
			NSSortDescriptor(key: "id", ascending: true)
		]
		return try self.managedObjectContext.executeFetchRequest(request) as! [Photo]
	}
	
	
	
	public func fetchPhotos(page page:Int = 0, limit: Int = 20) throws -> [Photo] {
		let request = NSFetchRequest(entityName: String(Photo))
		request.fetchOffset = page * limit
		request.fetchLimit = limit
		request.sortDescriptors = [
			NSSortDescriptor(key: "id", ascending: true)
		]
		return try self.managedObjectContext.executeFetchRequest(request) as! [Photo]
	}
	
	//MARK: - Convenience functions for insertNewObjectForEntityForName
	
	public func newPhoto() -> Photo {
		return NSEntityDescription.insertNewObjectForEntityForName(String(Photo), inManagedObjectContext: managedObjectContext) as! Photo
	}
	
	public func fetchOrCreatePhotoWithId(id: Int16) throws -> Photo {
		let photo = try fetchPhotoWithId(id)
		if let photo = photo {
			return photo
		} else {
			let nPhoto = newPhoto()
			nPhoto.id = id
			return nPhoto
		}
	}
	
	public func fetchPhotoWithId(id: Int16) throws -> Photo? {
		let request = NSFetchRequest(entityName: String(Photo))
		request.predicate = NSPredicate(format: "id == %@", String(id))
		let result = try self.managedObjectContext.executeFetchRequest(request) as! [Photo]
		return result.first
	}
}
