//
//  Errors.swift
//  WerkinApp
//
//  Created by Γιώργος Καϊμακάς on 07/06/16.
//  Copyright © 2016 gkaimakas. All rights reserved.
//

import Foundation

public class Errors {
	public static var ResponseSerializableError: NSError{
		return NSError(domain: "com.gkaimas.WerkinAppModels", code: 1000, userInfo: [
			NSLocalizedDescriptionKey : "Response Serialization Failed"
			])
	}
	
	public static var PhotoCacheEmptyError: NSError {
		return NSError(domain: "com.gkaimas.WerkinAppModels", code: 1001, userInfo: [
			NSLocalizedDescriptionKey : "The photo cache is empty"
			])
	}
}
