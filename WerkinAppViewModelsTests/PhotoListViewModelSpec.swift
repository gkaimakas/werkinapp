//
//  PhotoListViewModelSpec.swift
//  WerkinApp
//
//  Created by Γιώργος Καϊμακάς on 08/06/16.
//  Copyright © 2016 gkaimakas. All rights reserved.
//

import Quick
import Nimble
import Foundation
import ReactiveCocoa
import Result
import WerkinAppModels
@testable import WerkinAppViewModels

class PhotoListViewModelSpec: QuickSpec {
	override func spec() {
		super.spec()
		
		let stack = WerkinAppModelStack()
		let repository = PhotoRepository(stack: stack)
		let viewModel = PhotoListViewModel(repository: repository)
		
		describe("fetchPhotos") {
			it("should fetch a list of PhotoViewModels") {
				
				var list: [PhotoViewModel] = []
				viewModel
					.fetchPhotos()
					.on(next: { result in
						list.append(result)
					})
					.start()
				
				expect(list.count).toEventually(equal(20))
				
			}
		}
	}
}
